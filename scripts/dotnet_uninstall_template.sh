#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "$0 <template repo name>"
    exit 1
fi

SERVICE_TEMPLATE_REPO_NAME=$1

echo unregistering dotnet service template $SERVICE_TEMPLATE_REPO_NAME

for name in `dotnet new -u | grep $SERVICE_TEMPLATE_REPO_NAME`; do
    dotnet new -u $name
done

echo dotnet template unregistered