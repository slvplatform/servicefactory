#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "$0 <service name>"
    exit 1
fi

SERVICE_NAME=$1

BITBUCKET_TEMPLATE_REPO_ORG_NAME=slvplatform

DOTNET_TEMPLATE_NAME=slavooangulartemplate


DOTNET_TEMPLATE_GIT_REPO_URL=https://bitbucket.org/$BITBUCKET_TEMPLATE_REPO_ORG_NAME/$DOTNET_TEMPLATE_NAME.git

CURRENT_DIR=`dirname "${BASH_SOURCE[0]}"`

echo -n Generating code repository content from template ...

$CURRENT_DIR/dotnet_install_new_template_from_git_url.sh $DOTNET_TEMPLATE_NAME $DOTNET_TEMPLATE_GIT_REPO_URL &> /dev/null
if [ $? != 0 ]; then
    echo Error while retrieving and installing service template
    exit 1
fi 

$CURRENT_DIR/dotnet_new_service_from_template.sh $DOTNET_TEMPLATE_NAME $SERVICE_NAME &> /dev/null
if [ $? != 0 ]; then
    echo Error while creating code from template
    exit 1
fi 

$CURRENT_DIR/dotnet_uninstall_template.sh $DOTNET_TEMPLATE_NAME &> /dev/null
if [ $? != 0 ]; then
    echo Error while uninstalling dotnet template
    exit 0 # this is not really important
fi 

echo " Done"