#!/bin/bash

if [[ $# -ne 2 ]]; then
    echo "$0 <service template name> <service name>"
    exit 1
fi

SERVICE_TEMPLATE_NAME=$1
SERVICE_NAME=$2

echo generating repository for $SERVICE_NAME with template $SERVICE_TEMPLATE_NAME ...

dotnet new $SERVICE_TEMPLATE_NAME -o $SERVICE_NAME > /dev/null

echo repository generated