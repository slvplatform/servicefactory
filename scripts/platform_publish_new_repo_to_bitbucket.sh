#!/bin/bash

if [[ $# -ne 4 ]]; then
    echo "$0 <username> <password> <team name> <service name>"
    exit 1
fi

BITBUCKET_USERNAME=$1
BITBUCKET_PASSWORD=$2

BITBUCKET_TEAM_NAME=$3
SERVICE_NAME=$4

CURRENT_DIR=`dirname "${BASH_SOURCE[0]}"`

echo -n Publishing repository on bitbucket: $BITBUCKET_TEAM_NAME/$SERVICE_NAME ...

$CURRENT_DIR/bitbucket_repo_create.sh $BITBUCKET_USERNAME $BITBUCKET_PASSWORD $BITBUCKET_TEAM_NAME $SERVICE_NAME &> /dev/null
if [ $? != 0 ]; then
    echo Error while creating bitbucket repository for the template
    exit 1
fi

$CURRENT_DIR/bitbucket_repo_init_and_push.sh $BITBUCKET_USERNAME $BITBUCKET_PASSWORD $BITBUCKET_TEAM_NAME $SERVICE_NAME &> /dev/null
if [ $? != 0 ]; then
    echo Error while pushing generated code to the remote repository
    exit 1
fi

echo " Done."