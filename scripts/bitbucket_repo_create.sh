#!/bin/bash
# api call to bitbucket to create a repository within the given team name that belongs to project CORE

if [[ $# -ne 4 ]]; then
    echo "create_repo.sh <username> <password> <team name> <service name>"
    exit 1
fi

USERNAME=$1
PASSWORD=$2
TEAM_NAME=$3
SERVICE_NAME=$4

echo "Creating repository \"$SERVICE_NAME\" in the team \"$TEAM_NAME\" ..."

CREATE_REPO_RESPONSE=`curl -H Content-Type:application/json \
 -u $USERNAME:$PASSWORD \
 -d '
 {
    "scm": "git", 
    "project": {
        "key": "CORE"
    },
    "is_private": "true",
    "name":"'$SERVICE_NAME'"
}' \
 https://api.bitbucket.org/2.0/repositories/$TEAM_NAME/$SERVICE_NAME`

if [[ "$CREATE_REPO_RESPONSE" != "{"*"}" ]]; then
    echo the bitbucket api response is not a valid json;
    exit 1
fi

if [[ $? -ne 0 ]]; then
    echo the call to create bitbucket repo has failed 
    exit 1
fi

echo $CREATE_REPO_RESPONSE

ERROR_RESPONSE=`echo $CREATE_REPO_RESPONSE | jq 'select(.type == "error") | .error.message'`

if [[ ! -z "${ERROR_RESPONSE}" ]]; then
    echo failed to create repository : $ERROR_RESPONSE
    exit 1
fi

echo "Bitbucket remote repository created."