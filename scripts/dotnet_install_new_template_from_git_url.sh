#!/bin/bash

if [[ $# -ne 2 ]]; then
    echo "$0 <template name> <template git clone url>"
    exit 1
fi

DOTNET_NEW_TEMPLATE_NAME=$1

TEMPLATE_REPO_URL=$2


echo installing dotnet template $DOTNET_NEW_TEMPLATE_NAME from $TEMPLATE_REPO_URL ...

WORK_DIR=`mktemp -d`

git clone $TEMPLATE_REPO_URL $WORK_DIR/$DOTNET_NEW_TEMPLATE_NAME

rm -rf $WORK_DIR/$DOTNET_NEW_TEMPLATE_NAME/.git

dotnet new -i $WORK_DIR/$DOTNET_NEW_TEMPLATE_NAME

echo "dotnet template installed."