#!/bin/bash

if [[ $# -ne 4 ]]; then
    echo "create_repo.sh <username> <password> <team name> <service name>"
    exit 1
fi

urlencode() {
    # urlencode <string>
    old_lc_collate=$LC_COLLATE
    LC_COLLATE=C
    
    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%%%02X' "'$c" ;;
        esac
    done
    
    LC_COLLATE=$old_lc_collate
}

echo Initializing repo and pushing to remote ...

USERNAME=$1
PASSWORD=$2

TEAM_NAME=$3
SERVICE_NAME=$4

pushd $SERVICE_NAME

if [ -d .git ]; then
    echo .git director already exists. git init skippd.
else
    git init
    git add .
    git commit -m "Initialized from template"
fi

USERNAME_ENCODED=`urlencode $USERNAME`
PASSWORD_ENCODED=`urlencode $PASSWORD`

git push https://$USERNAME_ENCODED:$PASSWORD_ENCODED@bitbucket.org/$TEAM_NAME/$SERVICE_NAME.git --all

if [ $? != 0 ]; then
    echo Failed to push to remote repository
    exit 1
fi

popd

echo Remote repo pushed.