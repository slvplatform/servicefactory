#!/bin/bash

if [[ $# -ne 5 ]]; then
    echo "$0 <namespace> <component name> <git repo url> <git branch>"
    exit 1
fi


NAMESPACE=$1
COMPONENT_NAME=$2
GIT_REPOSITORY_URL=$3
GIT_REPOSITORY_BRANCH=$4
DOTNET_RESTORE_SOURCES=$5

CURRENT_DIR=`dirname "${BASH_SOURCE[0]}"`
OKD_TEMPLATES_DIR=$CURRENT_DIR/okd

oc process -f OKD_TEMPLATES_DIR -n $NAMESPACE  

git@bitbucket.org:${ORGANISATION_NAME}/${COMPONENT_NAME}.git