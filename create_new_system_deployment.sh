#!/bin/bash

if [[ $# -ne 3 ]]; then
    echo "$0 <system code name> <system display name> <path to builder ssh private key>"
    exit 1
fi

SYSTEM_CODE_NAME=$1
SYSTEM_DISPLAY_NAME=$2
BUILDER_SSH_PRIVATE_KEY=$2

# BITBUCKET_TEAM_NAME=$3
# SERVICE_NAME=$4
# SERVICE_DESCRIPTION=$5

SCRIPTS_DIRECTORY_PATH=`dirname "${BASH_SOURCE[0]}"`/scripts

oc new-project $SYSTEM_CODE_NAME --display-name=$SYSTEM_DISPLAY_NAME

oc create secret generic builder-ssh-private-key --from-file=ssh-privatekey=$BUILDER_SSH_PRIVATE_KEY


todo:
1. create the namespace
2. add source secrets
3. add the namespace to the list of watched namespaces by jenkins
4. add platform objects: database, messagebus, auth service, and their secrets


# $SCRIPTS_DIRECTORY_PATH/dotnet_install_template_from_bitbucket.sh $DOTNET_TEMPLATE_BITBUCKET_TEAM_NAME $DOTNET_TEMPLATE_NAME
# if [ $? != 0 ]; then
#     echo Error while retrieving and installing service template
#     exit 1
# fi 
