# Deployment Automation

This document describes how the deployment automation is designed. That includes all openshift objects as:

1. build strategies
1. deployment pipelines
1. platform shared components
1. platform configuration
1. environment specific objects

## Summary

* Every component has it's own dedicated build project
* The build project has pipeline(s) that build, test and tag it
* It's easy to create a project for component where it's built. That's why we keep branch builds in separate projects
* The environment contains deployment pipelines that get triggered by successful build of certain label
* [optional] It may be, that a project will deploy a service from another project. e.g. prod will deploy from QA?
* Environment specific settings are managed per environment, hence the services will have setting files templated so that we keep the structure but with no data?

## Components

```plantuml
@startuml Architecture
skinparam componentStyle uml2

cloud "prod\n" {
    component "SVC A, Version: 1.1" as a_11_prod <<u-service>>
    component "SVC B, Version: 2.1" as b_21_prod <<u-service>>
    node "Kafka" as mb_prod <<message bus>>
    database "Mongo Db" as db_prod <<database>>

    a_11_prod --> mb_prod: uses
    a_11_prod --> db_prod: uses

    b_21_prod --> mb_prod: uses

    b_21_prod --> db_prod: uses
}

@enduml
```

### ImageStream tagging

These labels are work in progress.

1. latest - latest s2i build that passed unit tests
1. accepted - latest that also passed deployment and acceptance tests
1. ready - accepted that has gone through qa env deployment and tests
1. prod - deployed to prod


### Build, Test, Tag


```plantuml
@startuml

(*) --> "Code has changed"
--> "s2i built and unit tests"

if "build and test succeeded?"
  -->[yes] "deploy for tests"
  --> "run full acceptance tests"
  if "deploy and acceptance test succeeded?"
    --> [yes] "Tag as tested"
    --> (*)
  else
      --> [no] (*)
  endif
else

--> [no] (*)
endif
```

### Deploy to QA

```plantuml
@startuml
skinparam componentStyle uml2

(*) --> "New 'TESTED' tag in image stream"
--> "Deploy the image"
--> "Trigger smoke tests"
if "Smoke tests succeeded?"
  -->[yes] "Tag as 'RC'"
  --> (*)
else
  -->[no] "Tag as 'SMOKED'"
endif
--> (*)
@enduml
```

## Deploy to Prod (low traffic)

```plantuml
@startuml
skinparam backgroundColor lightgreen
skinparam componentStyle uml2

(*) --> "New 'RC' tag in image stream"
--> "Deploy to canary"
--> "Trigger smoke tests with {[target]='canary'} tag"
if "Smoke tests succeeded?"
  -->[yes] "wait 10 minutes"
  --> "compare stats to baseline"
  if "stats within error margin"
    --> [yes] "deploy to the rest of the nodes"
    --> "Tag as 'PROD'"
    --> (*)
  else
    --> [no] "remove the canary node"
    --> "tag as 'PRODCANARYSTATSFAIL"
    --> (*)
  endif
else
  -->[no] "Tag as 'PRODCANARYFAIL'"
  --> (*)
endif
--> (*)
@enduml
```

## Open Questions

* How many namespaces do we create

## Build

The component is built using