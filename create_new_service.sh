#!/bin/bash

if [[ $# -ne 4 ]]; then
    echo "$0 <service name> <team name> <username> <password>"
    exit 1
fi

SERVICE_NAME=$1
BITBUCKET_TEAM_NAME=$2
BITBUCKET_USERNAME=$3
BITBUCKET_PASSWORD=$4

SCRIPTS_DIR=`dirname "${BASH_SOURCE[0]}"`/scripts

$SCRIPTS_DIR/platform_generate_repo_for_dotnet_service.sh $SERVICE_NAME
if [ $? != 0 ]; then
    echo Error while retrieving and installing service template
    exit 1
fi

$SCRIPTS_DIR/platform_publish_new_repo_to_bitbucket.sh $BITBUCKET_USERNAME $BITBUCKET_PASSWORD $BITBUCKET_TEAM_NAME $SERVICE_NAME
if [ $? != 0 ]; then
    echo Error while pushing generated code to the remote repository
    exit 1
fi
