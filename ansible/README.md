# Serial Tech-Prenour

Enables serial technology entreprenours to prototype business rapidly.

## Background

In order to prototype an idea, ppl often cut corners by ignoring concepts like security, architecture, automation, code quality, testing, etc.. This approach, although it allows for quick delivery of a simple prototype, it also introduces large amount of technical debt, which the companies later struggle to tackle while expanding at the same time. This project aims to mitigate the issue by providing automation to quickly establish a baseline platform that already incorporates all the most ignored principles.

## Vision

One day, when I have an idea, I will simply go to the template, type in the name, description and some basics like if it's a public open source project or private system, click confirm and half an hour later, enjoy a fully compliant system platform. That platform would have:

1. identity management
1. docker container registry
1. git repository
1. wiki
1. binary repositories
1. database and message bus
1. easy way of adding and removing fully compliant services
   1. security
   1. logging
   1. test automation
   1. build and deployment automation
   1. standardised design
1. monitoring, logging, auditing capability with log retention defined
1. platform tests

Also..

1. The platform will have a well thought through business integration, so that we are sure that the new idea can be developed as a new startup isolated from other startups.

## todo

1. firewall
1. autoprovision keycloak
1. integrate gitea with keycloak

1. binary repository. Nexus OSS or some other alternative. We need a place for:
   * nugets (questionably as we could just use nuget.org)
   * npms (as above)
   * docker images
   * possibly java at some point
   * other binary blobs

## Installation

On the control host you need to first run the following commands as root.

1. apt install python3
1. apt install python3-pip
1. pip3 install ansible
1. pip3 install netaddr

Then you need to make sure that the user account that you will use to manage the cluster has ssh key driven access to all public hosts.

## todo - final cleanup

1. fix cross referenced password for ldap which is hardcoded in keycloak
1. make the org name, e.g. slavoo.com, templatable. Use some clever logic for the ldap notation

## other ides

1. I guess we can run all shared services per product. If we want to, we can unify it all into one big managed system by using ldap to store all identity information.

## Ansible good practices

1. Build reusable modules
1. Put default values for variables owned by the role in the defaults/main.yaml, and default values of roles used within the role in the vars/main.yaml. It's all about which role owns the variable.

## Basic structure

* roles
  * ```role name```
    * main.yaml

      ``` yaml
      - name: "Install Kubernetes"
      shell: hostname

      - name: "Install packages"
        apt: pkg={{ item }} state=installed
        with_items:
        - git
        - vim
        - ...

      - name: "create dir"
        file: path=/opt/files state=directory owner=nobody group=nogroup mode=0755

      - name: "copy files to the hosts"
        template: src=../dir/file.template.txt dest=/opt/files owner=root group=root mode=0644 # the src file can use ansible templating

      - name: 'run shell if file not exists'
        shell: creates=/etc/bash.rc touch /etc/bash.rc
      ```

* ansible.cfg - contains url to the hosts
* hosts - contains the host names
* playbook.yaml - can be anywhere

  ``` yaml
  ---
  - hosts: all
  roles:
  - kubernetes
  ```
