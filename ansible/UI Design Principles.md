# Start Up Touch

Online system for that enables entrepronours and technlogists to setup their IT infrastructure in one click

## Tagline

All your digital infrastructure needs to run a company ready to use in 20 minutes. Just give us some info and choose how risky you can get and we'll give you the price and quality noone else will be able to compete with

## Main functionality

The system will allow the user to quickly setup a company integrated infrastructure and then scale it as needed. Parameters required would be:

1. Number of employees in the next year, and 5 years - if there's more then 100, we should maybe increase numbers of servers
1. Expected traffic - purely for scalability. We will be notifying users if their infrastructure is cracking and could use an increase in infrastructure
1. Risk apetite - (with suggestions depending on the number of employees). If you're good with the servers being down for half an hour on occasion, then one server is enough. If not, we need to start setting up load balancing, resiliance, etc... which will increase the price. Up to you.

## What will the user be able to do from the main screen

1. See the high level infrastructure
1. See the high level utilization
1. See the availability history
1. See the backup status (past few days + the weekend backup with tests)
1. Manage users
   1. add/remove/edit
   1. assign roles and projects
1. Manage projects
   1. create projects
   1. assign ppl to projects
   1. view project repositories with statuses (high level status - name, version/env, traffic, errors, latency ...)

## Visual research board

... todo ...