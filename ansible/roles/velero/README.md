# Backup/Restore - Velero with Restic

This role sets up velero with restic integration for volume backups and s3 integration for backup storage.

## Open Questions

1. Should we use helm to install?
1. Does Restic need to be installed on its own?
1. How should restore be triggered? - probably a command line. Maybe when we create a cluster and the velero instalation finds some back-ups available, it can suggest commands to run to restore from it.

## Design Decisions

1. S3 will be used for backup destination
1. For the time being Minio will be provide S3 capability
1. Restic will be used to back-up the volumes
1. We introduced a clear separation between PAAS (re-creatable stuff) and SAAS (stuff that needs to be restored on top of PAAS)
   1. Anything that has data that need to be perserved must be SAAS
   1. PAAS is pretty much everything that defines CRDs and doesn't have persisted storate otherwise
      1. k8s, kubedb, helm, ingress controller, cert-manager, velero, maybe tekton
