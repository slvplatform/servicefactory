# Identity and Access Management

## Useful Commands

Get Keycloak access token

``` bash
export KEYCLOAK_TOKEN=`curl -s -X POST "http://keycloak.{{ {{saas_domain_name }}}}/auth/realms/master/protocol/openid-connect/token"  -H "Content-Type: application/x-www-form-urlencoded"  -d "username=admin"  -d "password=keycloak123"  -d 'grant_type=password'  -d 'client_id=admin-cli' | jq -r '.access_token'`
```

make sure the token is allowed to create realms.

Post realm.json

``` bash
curl -s --oauth2-bearer $KEYCLOAK_TOKEN -H"Content-Type: application/json" http://keycloak.{{ saas_domain_name }}/auth/admin/realms/ -d @realm.json
```
