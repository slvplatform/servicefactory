# Continues Delivery

This document describes how the ci is implemented with the use of the flux, environments repository, directories and branches.

## Structure

1. each CD enabled project will contain the environments repo
1. the environments repo will contain
   1. 'artifacts' folder with kustomizations of all available components
   1. 'test|stage|prod' folders with configurations per env (stage only if needed)
   1. secrets encrypted

## Environments

* stage - should be as close to prod as possible. It's a test ride for deployment to prod anything that goes to prod needs to be first deployed here
* test - this is for testing integrations for other companies or for our test software
* prod - this is the prod
