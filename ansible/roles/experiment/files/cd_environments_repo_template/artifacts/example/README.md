# Artifacts

Each artifact representing some for of installable kubernetes object should have it's logical folder. The folder must contain the kustomization.yaml file with all the necessary options, but at least with the list of resources that should be included if the artifact is linked.

To use the artifact, you need to add path to that artifact folder to the list of bases of your desired environments.
