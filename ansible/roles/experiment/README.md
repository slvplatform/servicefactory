# Project Infrastructure Setup

This document describes what happens when the project infrastructure setup automation is triggered.

## Open Questions

1. Should we go with istio?
1. Should we install database by default?
1. Should we install kafka or something for messages by default?
1. Is elk the right solution?
1. Is opentracing the right solution?
1. How do we implement promotion pipelines? Should it all be orchestrated by drone.io? Should we go with spinaker or something like that? How abou using operators as the deployment tool?

## Description

1. create organisation within ldap
1. create keycloak realm
1. install gitea
   1. configure gitea to wrk with keycloak authentication
1. install drone
   1. configure drone to automatically discover gitea
   1. configure drone to authenticate against keycloak
1. prepare integration infrastructure
   1. create namespace
   1. setup elk or something
   1. have a think about istio
   1. opentracing
1. prepare production infra
   1. create namespace
   1. setup elk or something
   1. have a think about istio
   1. opentracing

## Pre-Design

Before we do anything, we answer these 3 questions:

### Describe the project

1. What is it?
2. Where does it live?
3. Who is it for?

### Words and pictures

Do a little research of pictures relevant to the product. Put them all together and try to extract a color palette.

Build a cloud of words that will help define the product.

Think of a name for it that fits all of it.
