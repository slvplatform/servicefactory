# Gitea

Used as a private github per product.

## TODO

1. add tests
   1. can clone via ssh
   1. can clone via https
1. Disable direct login - one must go via openid

## Discussions

### Should we use sqlite3 instead of postgres

Probably yes.

Pros:

1. simpler deployment and manageent
1. simpler back up/restore strategy
1. smaller memory/cpu usage

Cons:

1. we may see some concurency issues - only for more then 100 day to day users
1. only works on a single node deployment
1. will require some work
