- name: validate setup_org inputs
  assert:
    that:
      - gitea_org is defined
      - gitea_org.name is defined
      - gitea_org.description is defined
      - gitea_org.description | length <= 260
      - gitea_org.teams is defined
      - gitea_admin_uid is defined
      - gitea_admin_password is defined

- include: sync_ldap_users.yaml

- name: Check if organisation {{ gitea_org.name }} exists
  uri:
    url: "https://{{ gitea_host }}/api/v1/orgs/{{ gitea_org.name }}"
    force_basic_auth: yes
    user: "{{ gitea_admin_uid }}"
    password: "{{ gitea_admin_password }}"
    status_code: [200, 404]
  register: existing_gitea_org

- name: Create the {{ gitea_org.name }} organisation
  uri:
    url: https://{{ gitea_host }}/api/v1/orgs
    method: POST
    force_basic_auth: yes
    user: "{{ gitea_admin_uid }}"
    password: "{{ gitea_admin_password }}"
    headers:
      Content-Type: application/json
    body_format: json
    body:
      username: "{{ gitea_org.name }}"
      full_name: "{{ gitea_org.full_name }}"
      description: "{{ gitea_org.description }}"
      visibility: "private"
    status_code: 201
  changed_when: True
  when: existing_gitea_org.status == 404

- name: Update the {{ gitea_org.name }} organisation
  uri:
    url: https://{{ gitea_host }}/api/v1/orgs/{{ gitea_org.name }}
    method: PATCH
    force_basic_auth: yes
    user: "{{ gitea_admin_uid }}"
    password: "{{ gitea_admin_password }}"
    headers:
      Content-Type: application/json
    body_format: json
    body:
      username: "{{ gitea_org.name }}"
      full_name: "{{ gitea_org.full_name }}"
      description: "{{ gitea_org.description }}"
      visibility: private
    status_code: 200
  changed_when: True
  when: >
    existing_gitea_org.status == 200 and 
    ( existing_gitea_org.json.description != gitea_org.description or
    existing_gitea_org.json.full_name != gitea_org.full_name )

- name: Setup the {{ gitea_org.name }} org
  include_tasks: setup_org_add_team.yaml
  vars:
    org_name: "{{ gitea_org.name }}"
  with_items: "{{ gitea_org.teams }}"
  loop_control:
    loop_var: org_team
