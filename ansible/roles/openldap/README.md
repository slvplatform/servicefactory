# OpenLDAP configuration

Openldap is the chosen provider of ldap. This role sets it up with the required domain name, basic automation users and groups.

## FAQ

* You first need to create users and then add them to the groups for the memberof to show up.

## Query the slavoo.local db
```
kubectl -n openldap exec openldap-7bf4c77f5-pfcsj -- ldapsearch -Dcn=admin,dc=slavoo,dc=local -w <password> -b dc=slavoo,dc=local
```