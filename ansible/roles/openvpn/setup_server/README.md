# OpenVpn

This role sets up the openvpn connectivity.

## ToDo

1. When the time comes, add server side client nodes.
1. Load Balancing of ovpn for server side resilience.
1. client-to-client connectivity - look into server.conf
1. delete the client files downloaded to localhost
